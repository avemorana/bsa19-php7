<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $presentation = "<h5><i>" . count($arena->all()) . "</i> fighters on arena</h5>";
        $presentation .= "<h3>Fighters:</h3><br>";

        foreach ($arena->all() as $item) {
            $presentation .= $item->getName();
            $presentation .= ": " . $item->getHealth() . ", " . $item->getAttack();
            $image = '<img src="https://bit.ly/' . $item->getImage() . '">';
            $presentation .= $image;
            $presentation .= '<br>';
        }

        return $presentation;
    }
}
