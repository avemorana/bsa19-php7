<?php

declare(strict_types=1);

namespace App\Task1;

class Fighter
{

    private $id;

    private $name;

    private $health;

    private $attack;

    private $image;

    public function __construct(int $id, string $name, int $health, int $attack, string $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->health = $health;
        $this->attack = $attack;
        $this->image = $image;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getAttack(): int
    {
        return $this->attack;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $health
     */
    public function setHealth($health): void
    {
        $this->health = $health;
    }

    /**
     * @param mixed $attack
     */
    public function setAttack($attack): void
    {
        $this->attack = $attack;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }
}
