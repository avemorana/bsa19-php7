<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $fighters;

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        if (count($this->fighters) > 0) {
            $mostPowerfulFighter = $this->fighters[0];
            foreach ($this->fighters as $fighter) {
                if ($fighter->getAttack() > $mostPowerfulFighter->getAttack()) {
                    $mostPowerfulFighter = $fighter;
                }
            }
            return $mostPowerfulFighter;
        } else {
            return null;
        }
    }

    public function mostHealthy(): Fighter
    {
        if (count($this->fighters) > 0) {
            $mostHealthyFighter = $this->fighters[0];
            foreach ($this->fighters as $fighter) {
                if ($fighter->getHealth() > $mostHealthyFighter->getHealth()) {
                    $mostHealthyFighter = $fighter;
                }
            }
            return $mostHealthyFighter;
        } else {
            return null;
        }
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
